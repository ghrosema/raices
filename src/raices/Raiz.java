package raices;
import java.text.DecimalFormat;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author drosema
 */
public class Raiz {

    double a, c, b, discriminante;
    boolean imaginarias;
    String raiz1, raiz2;

    public Raiz(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
        discriminante= b * b - 4 * a * c;
        imaginarias = false;
        calcular();
    }

    public String getRaiz1() {
        return raiz1;
    }

    public String getRaiz2() {
        return raiz2;
    }

    private void calcular() {
        if (discriminante < 0) {
            imaginarias = true;
            discriminante = -discriminante;
        }
        double rreal = -b / (2 * a);
        double rimag = (Math.sqrt(discriminante)) / (2 * a);
        DecimalFormat df = new DecimalFormat("0.00");
        
        if (imaginarias) {
            raiz1 = String.valueOf(df.format(rreal));
            raiz1 = raiz1 + " + " + String.valueOf(df.format(rimag)) + "i";
            raiz2 = String.valueOf(df.format(rreal));
            raiz2 = raiz2 + " - " + String.valueOf(df.format(rimag)) + "i";
        } else {
            raiz1 = String.valueOf(df.format(rreal + rimag));
            raiz2 = String.valueOf(df.format(rreal - rimag));
        }
    }
}
